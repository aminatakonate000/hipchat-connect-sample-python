## Setup environment
1. Install Mongodb. Open a new terminal, type `mongod` to launch mongo server;
2. Install Redis. Open a new terminal, type `redis-server` to launch redis server;
3. Install ngrok. Open a new terminal, type `ngrok http [-subdomain=xxxxxxx] 5000`;

## Launch python app
1. `cd` to the project root directory;
2. type `easy_install pip` to install the `pip` if you didn't install it yet;
3. type `pip install virtualenv` to install virtualenv if you didn't install it yet;
4. `virtualenv ../venv` to create a virtual python environment for this project;
5. `source ../venv/bin/activate`;
6. `pip install -r requirements.txt` to install dependencies;
7. Open a new terminal, type `export AC_BASE_URL=https://yourSubDomain.ngrok.io && python app.py` to run the app;