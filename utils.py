from ac_flask.hipchat.auth import tenant
from ac_flask.hipchat.clients import post
from ac_flask.hipchat.db import redis


def update_glance(room_id, glance_data, glance_key):
    token = tenant.get_token(redis, scopes=['view_room'])
    return post("%s/addon/ui/room/%s" % (tenant.api_base_url, room_id), {
            "glance": [
                {"content": glance_data, "key": glance_key}
            ]
        }, token)