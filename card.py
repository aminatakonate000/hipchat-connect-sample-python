from ac_flask.hipchat.auth import tenant
from ac_flask.hipchat.clients import post
from ac_flask.hipchat.db import redis


# class Card(object):

#     def __init__(self):
#         self.data = {}

#     def with_label(self, value, card_type="html"):
#         self.data["label"] = {
#             "value": value,
#             "type": card_type
#         }
#         return self

#     def with_lozenge(self, label, lozenge_type):
#         self.data["status"] = {
#             "type": "lozenge",
#             "value": {
#                 "label": label,
#                 "type": lozenge_type
#             }
#         }
#         return self

#     def with_icon(self, url, url2x):
#         self.data["status"] = {
#             "type": "icon",
#             "value": {
#                 "url": url,
#                 "url@2x": url2x
#             }
#         }
#         return self

def send_notification_card(card_data,
                           message,
                           color='yellow',
                           notify=False,
                           message_format='html'):
    token = tenant.get_token(redis)
    return post(
        "%s/room/%s/notification" % (tenant.api_base_url, tenant.room_id),
        {
            "color": color,
            "message": message,
            "notify": notify,
            "message_format": message_format,
            "card": card_data,
        },
        token
    )

# def send_notification(message):
#     token = tenant.get_token(redis)
#     return post("%s/room/%s/notification" % (tenant.api_base_url, tenant.room_id), {"message": message}, token)