import random
from pprint import pprint
from flask import Flask
from flask import render_template

from ac_flask.hipchat import Addon
from ac_flask.hipchat import room_client
from ac_flask.hipchat import addon_client
from ac_flask.hipchat import sender
from ac_flask.hipchat import context
from ac_flask.hipchat.auth import tenant
from ac_flask.hipchat.db import redis
from ac_flask.hipchat.glance import Glance

# mock data for testing and demo
from card import send_notification_card
from utils import update_glance
from mock import card_data


addon = Addon(app=Flask(__name__),
              key="test-addon",
              name="Twitter AddOn",
              allow_room=True,
              # static_folder='static',
              scopes=['send_notification', 'view_room'])


@addon.configure_page()
def configure():
    return "Thank you for installing Twitter Addon!"


@addon.webhook(event="room_enter")
def room_entered():
    room_client.send_notification('hi: %s' % sender.name)
    pprint(addon.descriptor)
    return '', 204


@addon.webhook(event="room_message", pattern='^/update')
def room_message():
    label = 'Update count: <b>{}</b>'.format(random.randint(1, 100))
    glance_data = Glance().with_label(label).with_lozenge(
        'progress', 'current').data
    addon_client.update_room_glance(
                                    'glance.key',
                                    glance_data, context['room_id'])
    pprint(addon.descriptor)
    return '', 204


@addon.route(False, rule="/refreshglance", methods=["GET"])
def refresh_glance():
    label = 'Update count: <b>{}</b>'.format(random.randint(1, 100))
    glance_data = Glance().with_label(label).with_lozenge(
        'progress', 'current').data

    # pprint(addon.descriptor)
    update_glance(tenant.room_id, glance_data, 'glance.key')
    return '', 204


@addon.glance(key='glance.key', name='Glance',
              target='webpanel.key', icon='static/glance.png')
def glance():
    label = 'Update count: <b>{}</b>'.format(random.randint(1, 100))
    pprint(addon.descriptor)
    return Glance().with_label(label).with_lozenge('progress', 'current').data


@addon.webpanel(key='webpanel.key', name='Twitter Detail')
def web_panel():
    # token = tenant.get_token(redis, scopes=['view_room'])
    print "sender ->", sender.id, sender.name, sender.mention_name
    # sender_ = sender.
    pprint(addon.descriptor)
    # return "This is a twitter msg content."

    signed_request = tenant.sign_jwt(tenant.id)
    return render_template(
                           'webpanel.html',
                           id=tenant.id,
                           roomId=tenant.room_id,
                           groupId=tenant.group_id,
                           groupName=tenant.group_name,
                           senderId=sender.id,
                           senderName=sender.name,
                           signed_request=signed_request)


@addon.route(False, rule="/sendcard", methods=["GET"])
def send_card():
    room_client.send_notification("A card is coming...")

    message = "This is a card from server!!!"
    send_notification_card(card_data, message)
    print "sending Card!!"
    # return render_template('webpanel.html')
    return '', 204


@addon.route(False, rule="/dialog")
def open_dialog():
    print "A dialog is opening!!!!!!!!!!!!!!!!!!!!!!!!!!"
    # token = tenant.get_token(redis, scopes=['view_room'])
    signed_request = tenant.sign_jwt(tenant.id)
    return render_template('dialog.html',
                           id=tenant.id,
                           roomId=tenant.room_id,
                           groupId=tenant.group_id,
                           groupName=tenant.group_name,
                           senderId=sender.id,
                           senderName=sender.name,
                           signed_request=signed_request)
    # return '', 204


@addon.route(False, rule="/opendialogfrombtn", methods=["POST"])
def open_dialog_from_btn():
    print "A dialog is opening!!!!!!!!!!!!!!!!!!!!!!!!!!"
    token = tenant.get_token(redis, scopes=['view_room'])
    return render_template('dialog.html',
                           id=tenant.id,
                           roomId=tenant.room_id,
                           groupId=tenant.group_id,
                           groupName=tenant.group_name,
                           senderId=sender.id,
                           senderName=sender.name,
                           signed_request=token)
    # return '', 204


# for testing
@addon.app.route("/web")
def test_web():
    return render_template('webpanel.html')
    # return "This is a twitter msg content."


if __name__ == '__main__':
    addon.run()
