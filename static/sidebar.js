/* add-on script */

$(document).ready(function () {
    // console.log("document.ready now");
    // $(function () {
    //     AP.register(
    //         {
    //             "myaddon-action-opensidebar": function (message) {
    //                 handleMessage(message);
    //             }
    //         }
    //     );
    // });

    $("#openDialog").click(function (event) {
        // AP.require('dialog', function (dialog) {
        //     dialog.open({
        //         key: "myaddon-dialog",
        //         hint: "some hint", //optional
        //         button: "btn",   // optional
        //         title: "dialog title" // optional
        //     })
        // });

        console.log('$("#openDialog").click!!!');
        $.ajax({
            type: 'POST',
            url: '/opendialogfrombtn',
            contentType: 'application/json',
            data: JSON.stringify({
                'oauth_client_id': id
            }),
            success: function (data, textStatus, xhr) {
                console.log("glance refreshed!");
            },
            error: function () {
                console.log("error refreshing glance!");
            }
        });
    });

    $("#refreshGlance").click(function (event) {

        // headers: { 'x-acpt': signed_request },
        // dataType: 'json',
        // 'signed_request': signed_request,
        console.log('$("#refreshGlance").click!!!');
        $.ajax({
            type: 'GET',
            url: '/refreshglance',
            // contentType: 'application/json',
            // data: JSON.stringify({
            //     'oauth_client_id': id
            // }),
            data: {
                'signed_request': signed_request
            },
            success: function (data, textStatus, xhr) {
                console.log("glance refreshed!");
            },
            error: function () {
                console.log("error refreshing glance!");
            }
        });
    });

    $("#sendCard").click(function (event) {
        //posting a card is done server-side
        //for any POST to the add-on server, make sure to include the signed request (passed as a URL param signed_request
        //to the page)

        // headers: { 'x-acpt': signed_request },
        // dataType: 'json',
        // 'signed_request': signed_request,
        console.log('$("#sendCard").click!!!');
        $.ajax({
            type: 'GET',
            url: '/sendcard',
            // contentType: 'application/json',
            // data: JSON.stringify({
            //     'oauth_client_id': id
            // }),
            data: {
                'signed_request': signed_request
            },
            success: function (data, textStatus, xhr) {
                console.log("card posted");
            },
            error: function () {
                console.log("error posting card");
            }
        });
    });
});

// function handleMessage(message) {
//     $("#sidebarMessageBodyDiv").text(message.body);
//     $("#sidebarMessageSenderDiv").text(message.sender);
//     $("#sidebarMessageSenderMentionDiv").text(message.sender_mention);
//     $("#sidebarMessageSenderIdDiv").text(message.sender_id);
// }
