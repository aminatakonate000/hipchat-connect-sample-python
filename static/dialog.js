/* add-on script */

$(document).ready(function () {

    $(function() {
           AP.register({
                "myaddon-action-opendialog": function (message) {
					$("#dialogSource").text("Dialog opened from a message action");
                    handleDialogMessage(message);
                },
				"myaddon-inputaction-opendialog": function() {
					$("#dialogSource").text("Dialog opened from an input action");
				}
            });
    });

    $("#openSidebar").click(function (event) {
        //Opening a sidebar from a dialog is not yet possible
        //AP.require('sidebar', function (dialog) {
        //    dialog.open({
        //        key: "myaddon-sidebar"
        //    })
        //});
		alert("Operation not yet supported");

    });

    $("#updateGlance").click(function (event) {
        //Push a glance update from the server. This updates all clients with the integration sidebar open
        //for any POST to the add-on server, make sure to include the signed request (passed as a URL param signed_request
        //to the page)

        // headers: { 'x-acpt': signedRequest },
        $.ajax({
            type: 'POST',
            url: '/updateglance',
            dataType: 'json',
            data: {
                value: $("#dialogGlanceValue").val()
            },
            success: function (data, textStatus, xhr) {
                console.log("card posted");
            },
            error: function () {
                console.log("error posting card");
            }
        });
    });

});


function handleDialogMessage(message) {
    $("#dialogMessageBodyDiv").text(message.body);
    $("#dialogMessageSenderDiv").text(message.sender);
    $("#dialogMessageSenderMentionDiv").text(message.sender_mention);
    $("#dialogMessageSenderIdDiv").text(message.sender_id);
}
